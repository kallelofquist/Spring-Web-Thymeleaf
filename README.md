# Spring-Web-Thymeleaf

#### REST API and SQL assignment.

**By: Karl, Calle and Timothy**

* KARL - A bit of API, controller fix. Frontend stuff, Bootstrap and Thymeleaf. index.html.

* CALLE - Database and database handling. DbHandler.java. SQL statements.

* TIMOTHY - Controller. ContactController.java. Contact model. newcontact.html. show.html. editcontact.html. contact.html.