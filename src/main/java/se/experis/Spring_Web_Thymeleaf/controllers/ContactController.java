package se.experis.Spring_Web_Thymeleaf.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import se.experis.Spring_Web_Thymeleaf.model.Contact;
import org.springframework.ui.Model;
import se.experis.Spring_Web_Thymeleaf.util.DBHandler;

import java.util.List;

@Controller
public class ContactController {
    private final DBHandler dbHandler = new DBHandler();
    private List<Contact> contacts;



    /* --- LIST AND SEARCH FOR CONTACTS --- */

    /* Search for contact by name and return all matches */
    @RequestMapping(value = "/contact", method = RequestMethod.GET)
    public String getContactsByName(@RequestParam("name") String name,
                                    @RequestParam(name = "delete", required = false, defaultValue = "n") String delete,
                                    Model model) {
        contacts = dbHandler.selectByName(name);
        if (delete.equals("y")) {
            System.out.println(delete);
            model.addAttribute("showAlert", true);
        }
        else {
            model.addAttribute("showAlert", false);
        }

        model.addAttribute("contacts", contacts);
        model.addAttribute("showSearchResult", true);
        return "contact";
    }

    @GetMapping("/contact/all")
    public String getAllContacts(@RequestParam(name = "delete", required = false, defaultValue = "n") String delete,
                                 @RequestParam(name = "id", required = false, defaultValue = "0") String id,
                                 Model model) {
            contacts = dbHandler.selectAll();
        if (delete.equals("y")) {
            model.addAttribute("showAlert", true);
        }
        else {
            model.addAttribute("showAlert", false);
        }

        if (!id.equals("0")) {
            System.out.println("--- GETTING ID ---");
            model.addAttribute("contactIdentification", id);
        }

        model.addAttribute("contacts", contacts);
        model.addAttribute("showAll", true);
        return "contact";
    }

    /* --- ADD NEW CONTACT --- */

    @PostMapping("/contact/add")
    @ResponseStatus(HttpStatus.CREATED)
    public String createNewContact(@ModelAttribute("contact") Contact contact, Model model) {
        boolean couldAdd = false;
        if (!isInvalidContact(contact)) {
            couldAdd = dbHandler.add(contact.getName(), contact.getEmail(), contact.getContactNumber());

            if (!couldAdd) {
                model.addAttribute("newContactAlreadyExist", true);
            }
            else {
                model.addAttribute("newContactCreated", true);
            }

            model.addAttribute("name", contact.getName());
            return "newcontact";
        }
        else {
            throw new IllegalArgumentException("Error: Invalid contact fields!");
        }
    }

    @GetMapping("/contact/add")
    public String newContact(Model model) {
        model.addAttribute("contact", new Contact());
        return "newcontact";
    }

    /* --- DELETE CONTACT --- */

    @PostMapping("/contact/delete")
    public String deleteContact(@RequestParam("id") String id, Model model) {
        System.out.println(id);
        for (Contact contact : contacts) {
            if (contact.getContactNumber().equals(id)) {
                model.addAttribute("deletedName", contact.getName());
                contacts.remove(contact);
                model.addAttribute("contactDeleted", true);
                System.out.println("CONTACT DELETED");
                break;
            }
        }
        return "contact";
    }

    /* --- EDIT CONTACT --- */

    @PostMapping("/contact/edit")
    public String editContact(@RequestParam("originalNumber") String originalNumber, @ModelAttribute("contact") Contact contact, Model model) {
        dbHandler.update(contact.getName(), contact.getEmail(), contact.getContactNumber(), originalNumber);
        model.addAttribute("contactWasUpdated", true);
        model.addAttribute("name", contact.getName());
        return "editcontact";
    }

    @GetMapping("/contact/edit")
    public String getEditContact(@RequestParam("contactNumber") String contactNumber, Model model) {
        Contact contactToEdit = dbHandler.selectByContactNumber(contactNumber);
        model.addAttribute("contact", contactToEdit);
        return "editcontact";
    }


    /* EXCEPTION HANDLER */
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Error: A contact field was missing or incorrect.")
    @ExceptionHandler(IllegalArgumentException.class)
    public void badContactExceptionHandler() {
        // Nothing to do
    }

    /* Very Simple Contact Validation */
    private boolean isInvalidContact(Contact contact) {
        return contact == null || contact.getName() == null || contact.getName().isEmpty() ||
                contact.getEmail() == null || contact.getEmail().isEmpty() ||
                contact.getContactNumber() == null || contact.getContactNumber().isEmpty();
    }
}
