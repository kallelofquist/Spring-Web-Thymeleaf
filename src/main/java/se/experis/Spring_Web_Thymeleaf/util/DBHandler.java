   /*
    * This is the database handler. It will connect to the database, then either select
    * add, update or delete something in the database.
    * Note that contactNr must be unique and will be used as the identifier.
    * If we try to add, update or delete anything in the DB,
    * we use rollback as a safety measure if anything goes wrong.
    */

package se.experis.Spring_Web_Thymeleaf.util;

import se.experis.Spring_Web_Thymeleaf.model.Contact;
import java.sql.*;
import java.util.ArrayList;

public class DBHandler {
    private static String URL = "jdbc:sqlite::resource:database.db"; //URL to database
    private static Connection conn;
    private static ArrayList<Contact> contact;
    private static PreparedStatement preparedStatement;;
    private static boolean autoCommit;

    public void openConnection() {
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");
        } catch (Exception exception) {
            System.out.println(exception.toString());
        }
    }

    public void closeConnection() {
        try {
            conn.close();
            System.out.println("Connection to SQLite has been closed.");
        } catch (Exception exception) {
            System.out.println(exception.toString());
        }
    }

    //Selects a contant by contact name.
    public ArrayList<Contact> selectByName(String name) {
        openConnection();
        String sql = "SELECT contactName, contactEmail, contactNr " + " FROM contact " + " WHERE contactName LIKE ?";

        try {
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, "%" + name + "%"); //Allow the user to search for only a part of the String. For example will the serach result 'son' include everything with 'son
            ResultSet resultSet = preparedStatement.executeQuery();

            contact = new ArrayList<>();
            while (resultSet.next()) {
                contact.add(
                        new Contact(
                                resultSet.getString("contactName"),
                                resultSet.getString("contactEmail"),
                                resultSet.getString("contactNr")
                        ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
            return contact;
        }
    }

    //Selects a contact by contact number.
    public Contact selectByContactNumber(String contactNumber) {
        openConnection();
        String sql = "SELECT contactName, contactEmail, contactNr " + " FROM contact " + " WHERE contactNr = ?";

        Contact contact = new Contact();
        try {
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, contactNumber); //Allow the user to search for only a part of the String. For example will the serach result 'son' include everything with 'son
            ResultSet resultSet = preparedStatement.executeQuery();

            contact = new Contact(
                    resultSet.getString("contactName"),
                    resultSet.getString("contactEmail"),
                    resultSet.getString("contactNr")
            );
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
            return contact;
        }
    }

    //Selects everything in the DB
    public ArrayList<Contact> selectAll() {
        openConnection();
        String sql = "SELECT contactName, contactEmail, contactNr " + " FROM contact";

        try {
            preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();

            contact = new ArrayList<>();
            while (resultSet.next()) {
                contact.add(
                        new Contact(
                                resultSet.getString("contactName"),
                                resultSet.getString("contactEmail"),
                                resultSet.getString("contactNr")
                        ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
            return contact;
        }
    }

    //Adds new Contact in the DB. contactNr is the identifier.
    public boolean add(String name, String email, String contactNr) { //Fix throw
        openConnection();
        String sql = "INSERT INTO contact " + "VALUES (?, ?, ?)";

        try {
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, email);
            preparedStatement.setString(3, contactNr);

            autoCommit = conn.getAutoCommit();

            //Rollback
            try {
                // Turn off auto commit
                conn.setAutoCommit(false);

                // Do the transaction
                preparedStatement.execute();

                // If it succeed we commit the changes
                conn.commit();
            } catch (SQLException exc) {
                // Something went wrong, undo the partial changes
                conn.rollback();
                System.out.println("Rollback");
                closeConnection();
                return false;
            } finally {
                // Revert the DB auto commit setting
                conn.setAutoCommit(autoCommit);
                closeConnection();
                return true;
            }
        } catch(SQLException e){
            e.printStackTrace();
        }
        closeConnection();
        return false;
    }

    //Updates an exisiting row in DB. contactNr is the identifier, then it uses rowid to update, so it does not get a conflift with the new updated contact number
    public boolean update(String name, String email, String contactNr, String value) {
        openConnection();
        String sqlSelect = "SELECT rowid, contactName, contactEmail, contactNr " + " FROM contact " + " WHERE contactNr = ?";
        String sql = "UPDATE contact " + " SET contactName = ?, contactEmail = ?, contactNr = ? " + " WHERE rowid = ?";

        try {
            preparedStatement = conn.prepareStatement(sqlSelect);
            preparedStatement.setString(1, value);
            ResultSet resultSet = preparedStatement.executeQuery();

            int rowid = resultSet.getInt("rowid");

            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, email);
            preparedStatement.setString(3, contactNr);
            preparedStatement.setInt(4, rowid);

            autoCommit = conn.getAutoCommit();

            //Rollback
            try {
                conn.setAutoCommit(false);
                preparedStatement.executeUpdate();
                conn.commit();
            } catch (SQLException exc) {
                conn.rollback();
                System.out.println("Rollback");
                closeConnection();
                return false;
            } finally {
                conn.setAutoCommit(autoCommit);
                closeConnection();
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        closeConnection();
        return false;
    }

    //Deletes an exisiting row in DB. contactNr is the identifier.
    public boolean delete(String value) {
        openConnection();
        String sql = "DELETE " + " FROM contact  " + " WHERE contactNr = ?";

        try {
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, value);

            autoCommit = conn.getAutoCommit();

            //Rollback
            try {
                conn.setAutoCommit(false);
                preparedStatement.execute();
                conn.commit();
            } catch (SQLException exc) {
                conn.rollback();
                System.out.println("Rollback");
                closeConnection();
                return false;
            } finally {
                conn.setAutoCommit(autoCommit);
                closeConnection();
                return true;
            }
        } catch (SQLException e) {
                e.printStackTrace();
        }
        closeConnection();
        return false;
    }
}
